import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  newItem: string;
  itemCount: number;
  items = [];

  constructor() {
    this.itemCount = this.items.length;
  }

  addItem() {
    this.items.push(this.newItem);
    this.newItem = "";
    this.itemCount = this.items.length;
  }

  removeItem(index: number) {
    this.items.splice(index, 1);
    this.itemCount = this.items.length;
  }

}
